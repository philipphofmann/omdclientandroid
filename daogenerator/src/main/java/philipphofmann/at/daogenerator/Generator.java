package philipphofmann.at.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {

    //private static final String TARGET_FOLDER = "../app/src/main/java/";
    private static final String TARGET_FOLDER = "./app/src/main/java/";

    private static final int SCHEMA_VERSION = 1;

    public static final void main(String[] args) throws Throwable {
        Schema schema = new Schema(SCHEMA_VERSION, "philipphofmann.at.moviebrowser.greendao.generated");

        //Movie Search
        Entity movieSearch = schema.addEntity("MovieSearch");
        movieSearch.setTableName("MOVIE_SEARCH");
        movieSearch.addIdProperty();
        movieSearch.addStringProperty("title");
        movieSearch.addStringProperty("response");
        movieSearch.addStringProperty("error");

        //Movies
        Entity movies = schema.addEntity("Movie");
        movies.setTableName("MOVIE");
        movies.addIdProperty();
        movies.addStringProperty("title");
        movies.addStringProperty("released");
        movies.addStringProperty("runtime");
        movies.addStringProperty("posterUrl");
        movies.addStringProperty("imdbID");
        Property movieSearchId = movies.addLongProperty("movieSearchId").notNull().getProperty();


        //One movie Search has many search results
        movieSearch.addToMany(movies, movieSearchId);

        //this allows accessing the movieSearch from the movieSearchResult directly
        movies.addToOne(movieSearch, movieSearchId);

        // we are generating the schema here
        new DaoGenerator().generateAll(schema, TARGET_FOLDER);
    }


}
