package philipphofmann.at.moviebrowser.datamapper;

import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;

/**
 * Maps json strings to a models.
 * Created by philipphofmann on 21/09/15.
 */
public interface JsonDataMapper {
    /**
     * Fills a {@link OmdMovie} with values of the passed json string.
     * @param json The json as string.
     * @return A {@link OmdMovie} with values of the passed json string.
     */
    OmdMovie map(String json);

    /**
     * Fills a {@link OmdMultipleSearch} with values of the passed json string.
     * @param json The json as string.
     * @return A {@link OmdMovie} with values of the passed json string.
     */
    OmdMultipleSearch mapMultiple(String json);
}
