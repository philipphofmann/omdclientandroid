package philipphofmann.at.moviebrowser.datamapper;

import java.util.ArrayList;
import java.util.List;

import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;
import philipphofmann.at.moviebrowser.greendao.generated.MovieSearch;

/**
 * Maps models to view models
 * Created by philipphofmann on 21/09/15.
 */
public interface ViewModelMapper  {
    /**
     * Maps the values of {@link OmdMovie}  to the view model {@link MovieViewModel}
     */
    MovieViewModel mapWebToViewModel(OmdMovie movie);

    /**
     * Maps a list of {@link OmdMovie} to a {@link ArrayList} of {@link MovieViewModel}
     */
    ArrayList<MovieViewModel> mapWebToViewModel(List<OmdMovie> movies);

    MovieSearchViewModel mapWebToViewModel(OmdMultipleSearch omdMultipleSearch);

    /**
     * Maps a {@link MovieSearch} to a {@link MovieSearchViewModel}
     */
    MovieSearchViewModel mapDBToViewModel(MovieSearch movieSearch);


    Movie mapViewModelToDB(MovieViewModel movieViewModel);

    MovieViewModel mapDBToViewModel(Movie movie);
}
