package philipphofmann.at.moviebrowser.datamapper;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.androidannotations.annotations.EBean;

import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;

/**
 * Created by philipphofmann on 21/09/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class JsonDataMapperImpl implements JsonDataMapper {

    private static final String TAG = JsonDataMapperImpl.class.getSimpleName();

    private Gson gson;

    public JsonDataMapperImpl() {
        this.gson = new Gson();
    }

    @Override
    public OmdMovie map(String json) {
        try {
            return gson.fromJson(json, OmdMovie.class);
        } catch (JsonSyntaxException ex) {
            Log.d(TAG, "Can't parse passed json string: " + json, ex);
            return null;
        }
    }

    @Override
    public OmdMultipleSearch mapMultiple(String json) {
        try {
            return gson.fromJson(json, OmdMultipleSearch.class);
        } catch (JsonSyntaxException ex) {
            Log.d(TAG, "Can't parse passed json string: " + json, ex);
            return null;
        }
    }
}
