package philipphofmann.at.moviebrowser.datamapper;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;
import philipphofmann.at.moviebrowser.greendao.generated.MovieSearch;

/**
 * Maps View Models for web request and db and vice versa.
 *
 * Created by philipphofmann on 21/09/15.
 */
@EBean(scope = EBean.Scope.Singleton)
public class ViewModelMapperImpl implements ViewModelMapper {

    @Override
    public MovieViewModel mapWebToViewModel(OmdMovie movie) {
        MovieViewModel viewModel = new MovieViewModel();

        if (movie != null) {
            viewModel.setTitle(movie.getTitle());
            viewModel.setReleased(movie.getReleased());
            viewModel.setRuntime(movie.getRuntime());
            viewModel.setPosterUrl(movie.getPosterUrl());
            viewModel.setImdbID(movie.getImdbID());
        }

        return viewModel;
    }

    @Override
    public ArrayList<MovieViewModel> mapWebToViewModel(List<OmdMovie> movies) {
        ArrayList<MovieViewModel> movieViewModels = new ArrayList<>();

        for (OmdMovie movie : movies) {
            movieViewModels.add(mapWebToViewModel(movie));
        }

        return movieViewModels;
    }

    @Override
    public MovieSearchViewModel mapDBToViewModel(MovieSearch movieSearch) {
        if (movieSearch == null) {
            return null;
        }

        MovieSearchViewModel movieSearchViewModel = new MovieSearchViewModel();
        movieSearchViewModel.setTitle(movieSearch.getTitle());
        movieSearchViewModel.setError(movieSearch.getError());
        movieSearchViewModel.setResponse(movieSearch.getResponse());

        movieSearchViewModel.setMovies(mapDbToViewModel(movieSearch.getMovieList()));

        return movieSearchViewModel;
    }

    @Override
    public MovieSearchViewModel mapWebToViewModel(OmdMultipleSearch omdMultipleSearch) {
        MovieSearchViewModel movieSearchViewModel = new MovieSearchViewModel();

        if (omdMultipleSearch != null) {
            movieSearchViewModel.setError(omdMultipleSearch.getError());
            movieSearchViewModel.setResponse(omdMultipleSearch.getResponse());

            movieSearchViewModel.setMovies(mapWebToViewModel(omdMultipleSearch.getMovieList()));
        }

        return movieSearchViewModel;
    }

    @Override
    public Movie mapViewModelToDB(MovieViewModel movieViewModel) {
        Movie dbMovie = new Movie();

        if (movieViewModel != null) {
            dbMovie.setTitle(movieViewModel.getTitle());
            dbMovie.setReleased(movieViewModel.getReleased());
            dbMovie.setRuntime(movieViewModel.getRuntime());
            dbMovie.setPosterUrl(movieViewModel.getPosterUrl());
            dbMovie.setImdbID(movieViewModel.getImdbID());
        }

        return dbMovie;
    }

    private ArrayList<MovieViewModel> mapDbToViewModel(List<Movie> movies) {


        ArrayList<MovieViewModel> movieViewModels = new ArrayList<>();

        for (Movie movie : movies) {
            movieViewModels.add(mapDBToViewModel(movie));
        }

        return movieViewModels;
    }

    @Override
    public MovieViewModel mapDBToViewModel(Movie movie) {
        MovieViewModel viewModel = new MovieViewModel();

        if (movie != null) {
            viewModel.setTitle(movie.getTitle());
            viewModel.setReleased(movie.getReleased());
            viewModel.setRuntime(movie.getRuntime());
            viewModel.setPosterUrl(movie.getPosterUrl());
            viewModel.setImdbID(movie.getImdbID());
        }

        return viewModel;

    }
}
