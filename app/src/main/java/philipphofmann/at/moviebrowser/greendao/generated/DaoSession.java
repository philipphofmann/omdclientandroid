package philipphofmann.at.moviebrowser.greendao.generated;

import android.database.sqlite.SQLiteDatabase;

import java.util.Map;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.AbstractDaoSession;
import de.greenrobot.dao.identityscope.IdentityScopeType;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.

/**
 * {@inheritDoc}
 * 
 * @see de.greenrobot.dao.AbstractDaoSession
 */
public class DaoSession extends AbstractDaoSession {

    private final DaoConfig movieSearchDaoConfig;
    private final DaoConfig movieDaoConfig;

    private final MovieSearchDao movieSearchDao;
    private final MovieDao movieDao;

    public DaoSession(SQLiteDatabase db, IdentityScopeType type, Map<Class<? extends AbstractDao<?, ?>>, DaoConfig>
            daoConfigMap) {
        super(db);

        movieSearchDaoConfig = daoConfigMap.get(MovieSearchDao.class).clone();
        movieSearchDaoConfig.initIdentityScope(type);

        movieDaoConfig = daoConfigMap.get(MovieDao.class).clone();
        movieDaoConfig.initIdentityScope(type);

        movieSearchDao = new MovieSearchDao(movieSearchDaoConfig, this);
        movieDao = new MovieDao(movieDaoConfig, this);

        registerDao(MovieSearch.class, movieSearchDao);
        registerDao(Movie.class, movieDao);
    }
    
    public void clear() {
        movieSearchDaoConfig.getIdentityScope().clear();
        movieDaoConfig.getIdentityScope().clear();
    }

    public MovieSearchDao getMovieSearchDao() {
        return movieSearchDao;
    }

    public MovieDao getMovieDao() {
        return movieDao;
    }

}
