package philipphofmann.at.moviebrowser.base;

/**
 * The callback interface of the {@link BaseAsyncTask}.
 * Implement this interface to be notified when the {@link BaseAsyncTask} finishes.
 *
 * Created by philipphofmann on 21/09/15.
 */
public interface AsyncTaskListener<T> {

    /**
     * Called when the {@link BaseAsyncTask} finishes.
     * @param result The result of the {@link BaseAsyncTask}.
     */
    void onFinished(AsyncTaskResult<T> result);
}
