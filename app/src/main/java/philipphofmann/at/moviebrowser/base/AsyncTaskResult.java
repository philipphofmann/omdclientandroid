package philipphofmann.at.moviebrowser.base;

/**
 * The result of an {@link BaseAsyncTask}.
 * Created by philipphofmann on 21/09/15.
 */
public class AsyncTaskResult<T> {
    
    private boolean isSuccess = false;

    private T result;

    private String errorMessage;


    //region Getter and Setter

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


    //endregion
}
