package philipphofmann.at.moviebrowser.base;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

/**
 * Use this abstract class when you execute {@link AsyncTask}.
 * Set a {@link AsyncTaskListener} before you execute the {@link BaseAsyncTask} to be called when
 * the {@link BaseAsyncTask} finishes.
 *
 * Created by philipphofmann on 19/09/15.
 */
public abstract class BaseAsyncTask<Params, Result> extends AsyncTask<Params, Void, AsyncTaskResult<Result>> {

    private WeakReference<AsyncTaskListener<Result>> listener;

    /**
     * The passed listener is called when the {@link BaseAsyncTask} finishes.
     * @param listener A {@link WeakReference} to the {@link AsyncTaskListener}
     */
    public synchronized void setListener(WeakReference<AsyncTaskListener<Result>> listener) {
        this.listener = listener;
    }

    @Override
    protected void onPostExecute(AsyncTaskResult<Result> resultAsyncTaskResult) {
        super.onPostExecute(resultAsyncTaskResult);

        if(listener != null && listener.get() != null) {
            listener.get().onFinished(resultAsyncTaskResult);
        }
    }
}
