package philipphofmann.at.moviebrowser.ui.movies.overview;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.ui.base.RecyclerViewAdapterBase;
import philipphofmann.at.moviebrowser.ui.base.ViewWrapper;

/**
 * Created by philipphofmann on 02/11/15.
 */
@EBean
public class MovieAdapter extends RecyclerViewAdapterBase<MovieViewModel, MovieItemView> {

    public interface Listener {
        /**
         * Gets invoked when the list item is clicked.
         */
        void onItemClicked(String imdbID);
    }

    @RootContext
    Context context;

    private Listener callback;

    public void setItems(List<MovieViewModel> movies) {
        this.items = movies;
    }

    /**
     * Set a listener if you want to be notified when a list item is clicked.
     */
    public void setCallback(Listener callback) {
        this.callback = callback;
    }

    @Override
    protected MovieItemView onCreateItemView(ViewGroup parent, int viewType) {
        return MovieItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<MovieItemView> holder, int position) {
        MovieItemView movieItemView = holder.getView();

        MovieViewModel movie = items.get(position);
        if(movie != null) {
            movieItemView.bind(movie);
            movieItemView.setCallback(callback);
        }
    }


}
