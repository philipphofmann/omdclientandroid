package philipphofmann.at.moviebrowser.ui.movies.overview;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import philipphofmann.at.moviebrowser.R;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;

/**
 * Created by philipphofmann on 02/11/15.
 */
@EViewGroup(R.layout.list_item_movie)
public class MovieItemView extends LinearLayout {

    private MovieAdapter.Listener callback;

    //region UI
    @ViewById(R.id.tvMovieName)
    TextView tvMovieName;

    @ViewById(R.id.rlMovieNameWrapper)
    RelativeLayout rlMovieNameWrapper;

    //endregion

    private MovieViewModel movieViewModel;

    public MovieItemView(Context context) {
        super(context);
    }

    public void bind(final MovieViewModel movieViewModel) {
        this.movieViewModel = movieViewModel;
        tvMovieName.setText(movieViewModel.getTitle());
    }

    public void setCallback(MovieAdapter.Listener callback) {
        this.callback = callback;
    }

    @Click(R.id.rlMovieNameWrapper)
    public void onItemClicked() {
        if(callback != null) {
            callback.onItemClicked(movieViewModel.getImdbID());
        }
    }
}
