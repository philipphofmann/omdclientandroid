package philipphofmann.at.moviebrowser.ui.movies.cachedresults;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import de.greenrobot.dao.query.LazyList;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapper;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapperImpl;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;
import philipphofmann.at.moviebrowser.ui.base.RecyclerViewAdapterBase;
import philipphofmann.at.moviebrowser.ui.base.ViewWrapper;
import philipphofmann.at.moviebrowser.ui.movies.overview.MovieItemView;
import philipphofmann.at.moviebrowser.ui.movies.overview.MovieItemView_;

/**
 * Created by philipp.hofmann on 11/11/2015.
 */
@EBean
public class CachedAdapter extends RecyclerViewAdapterBase<MovieViewModel, MovieItemView> {

    @RootContext
    Context context;

    @Bean(ViewModelMapperImpl.class)
    ViewModelMapper viewModelMapper;

    protected List<Movie> lazyItems;

    public void setItems(LazyList<Movie> movies) {
        this.lazyItems = movies;
    }

    @Override
    public int getItemCount() {
        if(lazyItems == null) {
            return 0;
        }

        return lazyItems.size();
    }

    @Override
    protected MovieItemView onCreateItemView(ViewGroup parent, int viewType) {
        return MovieItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<MovieItemView> holder, int position) {
        MovieItemView movieItemView = holder.getView();

        MovieViewModel movie = viewModelMapper.mapDBToViewModel(lazyItems.get(position));
        if (movie != null) {
            movieItemView.bind(movie);
        }
    }
}
