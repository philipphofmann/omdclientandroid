package philipphofmann.at.moviebrowser.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Base class for {@link RecyclerView.ViewHolder} when using android annotations.
 * Created by philipphofmann on 09/11/15.
 */
public class ViewWrapper<V extends View> extends RecyclerView.ViewHolder {

    private V view;

    public ViewWrapper(V itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView() {
        return view;
    }
}
