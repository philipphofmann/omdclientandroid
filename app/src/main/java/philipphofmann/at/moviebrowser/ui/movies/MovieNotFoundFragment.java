package philipphofmann.at.moviebrowser.ui.movies;

import android.support.v4.app.Fragment;

import org.androidannotations.annotations.EFragment;

import philipphofmann.at.moviebrowser.R;

/**
 * Displays a text that no movies with the given title were found.
 * <p/>
 * Created by philipphofmann on 21/09/15.
 */
@EFragment(R.layout.fragment_movie_not_found)
public class MovieNotFoundFragment extends Fragment {

}
