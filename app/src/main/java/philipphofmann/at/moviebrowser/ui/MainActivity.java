package philipphofmann.at.moviebrowser.ui;


import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;

import java.util.ArrayList;
import java.util.List;

import philipphofmann.at.moviebrowser.R;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapper;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapperImpl;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.provider.OmdDataProviderImpl;
import philipphofmann.at.moviebrowser.ui.movies.MovieDetailFragment;
import philipphofmann.at.moviebrowser.ui.movies.MovieNotFoundFragment_;
import philipphofmann.at.moviebrowser.ui.movies.cachedresults.MovieSearchHistoryFragment;
import philipphofmann.at.moviebrowser.ui.movies.cachedresults.MovieSearchHistoryFragment_;
import philipphofmann.at.moviebrowser.ui.movies.overview.MovieOverviewFragment_;

/**
 * The {@link android.app.Activity} that is started when the app launches.
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements SearchMovieFragment.SearchClickedListener, MovieOverviewFragment_.MovieOverviewListener {

    //region Manager
    @Bean
    OmdDataProviderImpl omdDataProvider;

    @Bean(ViewModelMapperImpl.class)
    ViewModelMapper viewModelMapper;
    //endregion

    @Override
    protected void onResume() {
        super.onResume();
        omdDataProvider.callMeOnResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        omdDataProvider.callMeOnPause();
    }

    @Override
    @Background
    public void onMovieSearchButtonClicked(String movieTitle) {
        MovieSearchViewModel movies = omdDataProvider.getMoviesByTitle(movieTitle);
        Fragment contentFragment = getContent(movies);
        replaceContentFragment(contentFragment);
    }

    @Override
    public void onMovieSearchHistoryButtonClicked() {
        Fragment contentFragment = new MovieSearchHistoryFragment_();
        replaceContentFragment(contentFragment);
    }

    @Override
    @Background
    public void onMovieItemClicked(String imdbID) {
        Fragment detailFragment = MovieDetailFragment.getInstance(imdbID);
        replaceContentFragment(detailFragment);
    }

    @UiThread
    private void replaceContentFragment(Fragment contentFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.flContainerMain, contentFragment);
        transaction.commitAllowingStateLoss();

        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private Fragment getContent(MovieSearchViewModel movie) {
        Fragment fragment = new MovieNotFoundFragment_();

        List<MovieViewModel> movies = movie.getMovies();
        if (movies != null && movies.size() > 0) {

            fragment = MovieOverviewFragment_.newInstance(new ArrayList<>(movies));
        }

        return fragment;
    }


}
