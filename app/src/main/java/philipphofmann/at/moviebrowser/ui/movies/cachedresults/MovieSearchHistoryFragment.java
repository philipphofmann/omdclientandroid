package philipphofmann.at.moviebrowser.ui.movies.cachedresults;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import de.greenrobot.dao.query.LazyList;
import philipphofmann.at.moviebrowser.R;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;
import philipphofmann.at.moviebrowser.provider.CacheDataProvider;
import philipphofmann.at.moviebrowser.provider.CacheDataProviderImpl;
import philipphofmann.at.moviebrowser.ui.movies.overview.MovieAdapter;

/**
 * Created by philipp.hofmann on 11/11/2015.
 */
@EFragment(R.layout.fragment_movie_list)
public class MovieSearchHistoryFragment extends Fragment {

    @ViewById(R.id.rvMovies)
    RecyclerView rvMovies;

    @Bean
    CachedAdapter adapter;

    @Bean(CacheDataProviderImpl.class)
    CacheDataProvider cacheDataProvider;

    private LazyList<Movie> allMovies;

    @AfterViews
    void bindAdapter() {
        rvMovies.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.onSaveInstanceState();

        rvMovies.setLayoutManager(layoutManager);

        cacheDataProvider.callMeOnResume();
        allMovies = cacheDataProvider.getAllMovies();
        adapter.setItems(allMovies);
        rvMovies.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        cacheDataProvider.callMeOnResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        cacheDataProvider.callMeOnPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(allMovies != null) {
            allMovies.close();
        }
    }
}
