package philipphofmann.at.moviebrowser.ui.movies;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.lang.ref.WeakReference;

import philipphofmann.at.moviebrowser.R;
import philipphofmann.at.moviebrowser.base.AsyncTaskListener;
import philipphofmann.at.moviebrowser.base.AsyncTaskResult;
import philipphofmann.at.moviebrowser.base.BaseAsyncTask;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapper;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapperImpl;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.provider.OmdDataProvider;
import philipphofmann.at.moviebrowser.provider.OmdDataProviderImpl;

/**
 * Shows detail info for a movie.
 * <p/>
 * Created by philipphofmann on 21/09/15.
 */
@EFragment(R.layout.fragment_movie_detail)
public class MovieDetailFragment extends Fragment {

    protected static final String ARG_MOVIE = "ARG_MOVIE";

    //region Business Components

    @Bean(OmdDataProviderImpl.class)
    OmdDataProvider dataProvider;

    @Bean(ViewModelMapperImpl.class)
    ViewModelMapper viewModelMapper;
    //endregion

    private MovieViewModel viewModel;
    private String imdbID;

    //region UI
    @ViewById(R.id.tvMovieDetailTitle)
    TextView tvMovieDetailTitle;

    @ViewById(R.id.tvMovieDetailReleased)
    TextView tvMovieDetailReleased;

    @ViewById(R.id.tvMovieDetailRuntime)
    TextView tvMovieDetailRuntime;

    @ViewById(R.id.ivMovieDetailPoster)
    ImageView ivMovieDetailPoster;
    //endregion

    public static MovieDetailFragment getInstance(String imdbID) {
        MovieDetailFragment fragment = new MovieDetailFragment_(); //Call android annotains constructor

        if (imdbID != null) {
            Bundle args = new Bundle();
            args.putString(ARG_MOVIE, imdbID);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {

            imdbID = args.getString(ARG_MOVIE);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        dataProvider.callMeOnResume();
        loadMovieDetails();
    }

    @Override
    public void onPause() {
        super.onPause();
        dataProvider.callMeOnPause();
    }

    @Background
    public void loadMovieDetails() {
        viewModel = dataProvider.getMovieByImdbID(imdbID);
        renderMovieData();
    }

    @UiThread
    public void renderMovieData() {
        if (viewModel != null) {
            tvMovieDetailTitle.setText(viewModel.getTitle());
            tvMovieDetailReleased.setText(viewModel.getReleased());
            tvMovieDetailRuntime.setText(viewModel.getRuntime());

            BaseAsyncTask<Void, Bitmap> imageByUrlTask = dataProvider.getImageByUrl(viewModel.getPosterUrl());
            imageByUrlTask.setListener(new WeakReference<AsyncTaskListener<Bitmap>>(posterImageListener));
            imageByUrlTask.execute();
        }
    }

    private AsyncTaskListener<Bitmap> posterImageListener = new AsyncTaskListener<Bitmap>() {
        @Override
        public void onFinished(AsyncTaskResult<Bitmap> result) {
            if (result != null && result.isSuccess()) {
                Bitmap image = result.getResult();

                if (image != null) {
                    ivMovieDetailPoster.setImageBitmap(image);
                }
            }
        }
    };
}
