package philipphofmann.at.moviebrowser.ui.movies.overview;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import philipphofmann.at.moviebrowser.R;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
/**
 * Displays a list of found movies.
 * <p/>
 * Created by philipphofmann on 21/09/15.
 */
@EFragment(R.layout.fragment_movie_list)
public class MovieOverviewFragment extends Fragment {

    public interface MovieOverviewListener {
        void onMovieItemClicked(String imdbID);
    }

    protected static final String ARG_MOVIES = "ARG_MOVIES";

    private MovieOverviewListener listener;

    private ArrayList<MovieViewModel> viewModel = new ArrayList<>();


    //region ui
    @ViewById(R.id.rvMovies)
    RecyclerView rvMovies;

    @Bean
    MovieAdapter adapter;

    //endregion

    public static MovieOverviewFragment newInstance(ArrayList<MovieViewModel> viewModel) {
        MovieOverviewFragment fragment = new MovieOverviewFragment_();

        if (viewModel != null) {
            Bundle args = new Bundle();

            args.putParcelableArrayList(ARG_MOVIES, viewModel);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {

            ArrayList<Parcelable> movies = args.getParcelableArrayList(ARG_MOVIES);
            fillViewModel(movies);
        }
        //Restore View State
        else if(savedInstanceState != null) {
            fillViewModel(savedInstanceState.getParcelableArrayList(ARG_MOVIES));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList(ARG_MOVIES, viewModel);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (MovieOverviewListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString()
                    + " must implement MovieOverviewListener");
        }
    }

    @AfterViews
    void bindAdapter() {
        rvMovies.setHasFixedSize(true);
        rvMovies.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.onSaveInstanceState();

        rvMovies.setLayoutManager(layoutManager);

        adapter.setItems(viewModel);
        rvMovies.setAdapter(adapter);

        adapter.setCallback(new MovieAdapter.Listener() {
            @Override
            public void onItemClicked(String imdbID) {
                listener.onMovieItemClicked(imdbID);
            }
        });
    }


    /**
     * Fill {@link MovieOverviewFragment#viewModel} with values from the passed movies.
     */
    private void fillViewModel(ArrayList<Parcelable> movies) {
        viewModel.clear();

        if (movies != null) {

            for(Parcelable movie : movies) {
                if(movie != null && movie instanceof MovieViewModel) {
                    viewModel.add((MovieViewModel) movie);
                }
            }
        }
    }
}
