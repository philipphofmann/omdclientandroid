package philipphofmann.at.moviebrowser.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import philipphofmann.at.moviebrowser.R;

/**
 * This fragment provides a {@link EditText} to enter a movie title and a {@link Button}. When
 * the {@link Button} is clicked the search for a movie is started.
 *
 * Created by philipphofmann on 21/09/15.
 */
@EFragment(R.layout.fragment_search_movie)
public class SearchMovieFragment extends Fragment {

    //region ui
    @ViewById(R.id.etMovieSearch)
    EditText etMovieSearch;

    @ViewById(R.id.bMovieSearch)
    Button bMovieSearch;
    //endregion


    private  SearchClickedListener listener;

    public interface SearchClickedListener {
        void onMovieSearchButtonClicked(String movieTitle);
        void onMovieSearchHistoryButtonClicked();
    }


    public SearchMovieFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (SearchClickedListener) activity;
        }
        catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString()
                    + " must implement SearchClickedListener");
        }
    }

    @Click(R.id.bMovieSearch)
    public void searchMovies(View bMovieSearch) {
        listener.onMovieSearchButtonClicked(etMovieSearch.getText().toString());
    }

    @Click(R.id.bMovieSearchHisotry)
    public void showSearchHistory(View bMovieSearchHisory) {
        listener.onMovieSearchHistoryButtonClicked();
    }

}
