package philipphofmann.at.moviebrowser.dataobjects.omd;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by philipphofmann on 02/11/15.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class OmdMultipleSearch extends BaseOmd {

    @JsonProperty("Search")
    private List<OmdMovie> movieList = new ArrayList<>();


    //region Getter and Setter

    public List<OmdMovie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<OmdMovie> movieList) {
        this.movieList = movieList;
    }

    //endregion


}
