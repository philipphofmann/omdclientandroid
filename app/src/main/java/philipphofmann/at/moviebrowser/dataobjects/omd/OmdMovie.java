package philipphofmann.at.moviebrowser.dataobjects.omd;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The model for a movie of the <a href="http://www.omdbapi.com/">OmdbApi</a>.
 * Created by philipphofmann on 19/09/15.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OmdMovie  extends BaseOmd {
    @JsonProperty("Title")
    private String title;

    @JsonProperty("Released")
    private String released;

    @JsonProperty("Runtime")
    private String runtime;

    @JsonProperty("Poster")
    private String posterUrl;

    @JsonProperty("imdbID")
    private String imdbID;


    public OmdMovie () {}


    //region Getter and Setter

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    //endregion
}
