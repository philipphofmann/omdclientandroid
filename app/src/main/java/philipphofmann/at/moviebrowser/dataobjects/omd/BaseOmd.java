package philipphofmann.at.moviebrowser.dataobjects.omd;

import com.google.gson.annotations.SerializedName;

/**
 * Base object for calls to <a href="http://www.omdbapi.com/">OmdbApi</a>.
 *
 * Created by philipphofmann on 02/11/15.
 */
public class BaseOmd {

    @SerializedName("Response")
    private String response;

    @SerializedName("Error")
    private String error;

    //region Getter and Setter

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    //endregion
}
