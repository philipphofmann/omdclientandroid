package philipphofmann.at.moviebrowser.dataobjects.view;

import java.util.List;

/**
 * Created by philipphofmann on 09/11/15.
 */
public class MovieSearchViewModel {
    private String title;

    private List<MovieViewModel> movies;

    private String response;

    private String error;

    //region Getter and Setter
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<MovieViewModel> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieViewModel> movies) {
        this.movies = movies;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    //endregion
}
