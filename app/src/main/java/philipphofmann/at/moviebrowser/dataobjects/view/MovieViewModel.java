package philipphofmann.at.moviebrowser.dataobjects.view;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * The view model for a movie.
 *
 * Created by philipphofmann on 21/09/15.
 */
public class MovieViewModel implements Parcelable {


    private String title;

    private String released;

    private String runtime;

    private String posterUrl;

    private String imdbID;

    public MovieViewModel() {

    }


    //region Getter and Setter

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getImdbID() {
        return imdbID;
    }

    public void setImdbID(String imdbID) {
        this.imdbID = imdbID;
    }

    //endregion

    //region Parcelable

    protected MovieViewModel(Parcel in) {
        title = in.readString();
        released = in.readString();
        runtime = in.readString();
        posterUrl = in.readString();
        imdbID = in.readString();
    }

    public static final Creator<MovieViewModel> CREATOR = new Creator<MovieViewModel>() {
        @Override
        public MovieViewModel createFromParcel(Parcel in) {
            return new MovieViewModel(in);
        }

        @Override
        public MovieViewModel[] newArray(int size) {
            return new MovieViewModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(released);
        dest.writeString(runtime);
        dest.writeString(posterUrl);
        dest.writeString(imdbID);
    }

    //endregion
}
