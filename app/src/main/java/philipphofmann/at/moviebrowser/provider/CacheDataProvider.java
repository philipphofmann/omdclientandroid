package philipphofmann.at.moviebrowser.provider;

import android.app.Activity;

import de.greenrobot.dao.query.LazyList;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;

/**
 * Saves search results in a cache.
 *
 * Created by philipphofmann on 09/11/15.
 */
public interface CacheDataProvider {

    /**
     * Call this method in the method {@link Activity#onResume()}
     */
    void callMeOnResume();

    /**
     * Call this method in the method {@link Activity#onPause()} ()}
     */
    void callMeOnPause();

    /**
     * Searches for a {@link MovieSearchViewModel} in the cache with the passed title. If more than
     * one search is found the first is taken.
     */
    MovieSearchViewModel getMovieSearch(String title);

    /**
     * Searches for a {@link MovieViewModel} in the cache with the passed imdbID. If more than
     * one movie is found the first is taken.
     */
    MovieViewModel getMovieByImdbID(String imdbID);

    /**
     * Saves a {@link MovieSearchViewModel} into the cache.
     */
    void createMovieSearch(MovieSearchViewModel movieSearchViewModel);


    /**
     * Updates the passed movie
     */
    void updateMovie(MovieViewModel movie);

    /**
     * Saves a {@link MovieViewModel} into the cache.
     */
    void createMovie(MovieViewModel movieViewModel);


    LazyList<Movie> getAllMovies();
}
