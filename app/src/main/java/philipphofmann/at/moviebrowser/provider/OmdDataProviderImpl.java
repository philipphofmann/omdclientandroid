package philipphofmann.at.moviebrowser.provider;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.rest.RestService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import philipphofmann.at.moviebrowser.base.AsyncTaskResult;
import philipphofmann.at.moviebrowser.base.BaseAsyncTask;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapper;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapperImpl;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;

/**
 * Created by philipphofmann on 21/09/15.
 */
@EBean
public class OmdDataProviderImpl implements OmdDataProvider {

    private static final String TAG = OmdDataProviderImpl.class.getSimpleName();

    @RootContext
    Context context;

    @RestService
    OmdDataProviderAnnotated omdDataProvider;

    @Bean(ViewModelMapperImpl.class)
    ViewModelMapper viewModelMapper;

    @Bean(OmdRequestUrlBuilderImpl.class)
    OmdRequestUrlBuilder urlBuilder;

    @Bean(CacheDataProviderImpl.class)
    CacheDataProvider cache;

    @Override
    public MovieViewModel getMovieByImdbID(String id) {

        MovieViewModel movieViewModel = cache.getMovieByImdbID(id);

        //Not in Cache
        if(movieViewModel == null) {
            OmdMovie movieByImdbID = omdDataProvider.getMovieByImdbID(id);
            movieViewModel = viewModelMapper.mapWebToViewModel(movieByImdbID);
            cache.createMovie(movieViewModel);
        }

        //If the movie has not all properties filled.
        if( movieViewModel.getPosterUrl() == null || movieViewModel.getReleased() == null || movieViewModel.getRuntime() == null) {
            OmdMovie movieByImdbID = omdDataProvider.getMovieByImdbID(id);

            movieViewModel.setPosterUrl(movieByImdbID.getPosterUrl());
            movieViewModel.setReleased(movieByImdbID.getReleased());
            movieViewModel.setRuntime(movieByImdbID.getRuntime());

            cache.updateMovie(movieViewModel);
        }

        return movieViewModel;
    }

    @Override
    public MovieSearchViewModel getMoviesByTitle(String title) {

        //Check if search is in cache
        MovieSearchViewModel movieSearch = cache.getMovieSearch(title);

        if (movieSearch != null) {
            return movieSearch;
        } else {
            //Get data from web
            OmdMultipleSearch moviesByTitle = omdDataProvider.getMoviesByTitle(title);

            //Fill view model
            MovieSearchViewModel movieSearchViewModel = viewModelMapper.mapWebToViewModel(moviesByTitle);
            movieSearchViewModel.setTitle(title);

            //Cache result in cache
            cache.createMovieSearch(movieSearchViewModel);

            return movieSearchViewModel;
        }
    }

    @Override
    public BaseAsyncTask<Void, Bitmap> getImageByUrl(final String imageUrl) {
        return new BaseAsyncTask<Void, Bitmap>() {
            @Override
            protected AsyncTaskResult<Bitmap> doInBackground(Void... params) {

                final AsyncTaskResult<Bitmap> result = new AsyncTaskResult<>();

                try {
                    URL url = new URL(imageUrl);
                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                    result.setResult(bmp);
                    result.setIsSuccess(true);
                } catch (MalformedURLException e) {
                    Log.d(TAG, "Could not format passed url:" + imageUrl, e);
                } catch (IOException e) {
                    Log.d(TAG, "Could not perform request.", e);
                }

                return result;
            }
        };
    }

    @Override
    public void callMeOnResume() {
        cache.callMeOnResume();
    }

    @Override
    public void callMeOnPause() {
        cache.callMeOnPause();
    }
}
