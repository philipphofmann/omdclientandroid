package philipphofmann.at.moviebrowser.provider;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMovie;
import philipphofmann.at.moviebrowser.dataobjects.omd.OmdMultipleSearch;

/**
 * Created by stefa_000 on 06.11.2015.
 */
@Rest(rootUrl = "http://www.omdbapi.com/", converters = {MappingJackson2HttpMessageConverter.class})
interface OmdDataProviderAnnotated {

    /**
     * Makes an json request to the <a href="http://www.omdbapi.com/">OmdbAPI</a> to the movies with
     * the given imdbID.
     * @param title The title of the movie to search for.
     */
    @Get("?i={title}&y=&plot=full&r=json")
    OmdMovie getMovieByImdbID(String title);

    /**
     * Makes an json request to the <a href="http://www.omdbapi.com/">OmdbAPI</a> to the movies with
     * the given title.
     * @param title The title of the movie to search for.
     */
    @Get("?s={title}&y=&plot=full&r=json")
    OmdMultipleSearch getMoviesByTitle(String title);

}
