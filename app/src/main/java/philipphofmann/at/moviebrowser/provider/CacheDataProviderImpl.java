package philipphofmann.at.moviebrowser.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

import de.greenrobot.dao.query.LazyList;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapper;
import philipphofmann.at.moviebrowser.datamapper.ViewModelMapperImpl;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;
import philipphofmann.at.moviebrowser.greendao.generated.DaoMaster;
import philipphofmann.at.moviebrowser.greendao.generated.DaoSession;
import philipphofmann.at.moviebrowser.greendao.generated.Movie;
import philipphofmann.at.moviebrowser.greendao.generated.MovieDao;
import philipphofmann.at.moviebrowser.greendao.generated.MovieSearch;
import philipphofmann.at.moviebrowser.greendao.generated.MovieSearchDao;

/**
 * As a cache GreenDao is used.
 *
 * Created by philipphofmann on 09/11/15.
 */
@EBean
public class CacheDataProviderImpl implements CacheDataProvider {

    private static final String DATABASE_NAME = "movies";

    @RootContext
    Context context;

    @Bean(ViewModelMapperImpl.class)
    ViewModelMapper viewModelMapper;

    private MovieSearchDao movieSearchDao;
    private MovieDao movieDao;

    // database connection / session related
    private SQLiteDatabase databaseConnection;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    @Override
    public void callMeOnResume() {
        databaseConnection = new DaoMaster.DevOpenHelper(context, DATABASE_NAME, null).getWritableDatabase();
        daoMaster = new DaoMaster(databaseConnection);
        daoSession = daoMaster.newSession(); // we can instantiate multiple sessions as well, sessions share the connection owned by the DaoMaster!

        movieSearchDao = daoSession.getMovieSearchDao();
        movieDao = daoSession.getMovieDao();
    }

    @Override
    public void callMeOnPause() {
        if (databaseConnection != null && databaseConnection.isOpen()) {
            databaseConnection.close(); // close cache connection if it's open
        }
    }

    @Override
    public MovieSearchViewModel getMovieSearch(String title) {
        List<MovieSearch> movieSearch = movieSearchDao.queryBuilder().where(MovieSearchDao.Properties.Title.eq(title)).list();

        if(movieSearch == null || movieSearch.size() == 0) {
            return null;
        }

        return viewModelMapper.mapDBToViewModel(movieSearch.get(0));
    }

    @Override
    public MovieViewModel getMovieByImdbID(String imdbID) {
        List<Movie> movies = getMoviesByID(imdbID);

        if(movies == null || movies.size() == 0) {
            return null;
        }


        return viewModelMapper.mapDBToViewModel(movies.get(0));
    }


    @Override
    public void createMovieSearch(MovieSearchViewModel movieSearchViewModel) {
        MovieSearch movieSearch = new MovieSearch();

        movieSearch.setTitle(movieSearchViewModel.getTitle());
        movieSearch.setResponse(movieSearchViewModel.getResponse());
        movieSearch.setError(movieSearchViewModel.getError());

        movieSearchDao.insert(movieSearch);

        for (MovieViewModel movieViewModel : movieSearchViewModel.getMovies()) {
            Movie dbMovie = viewModelMapper.mapViewModelToDB(movieViewModel);
            dbMovie.setMovieSearch(movieSearch);
            movieDao.insert(dbMovie);


            movieSearch.getMovieList().add(dbMovie);

        }

        movieSearch.update();
    }

    @Override
    public void updateMovie(MovieViewModel movieViewModel) {
        if(movieViewModel == null) {
            return;
        }

        List<Movie> movies = getMoviesByID(movieViewModel.getImdbID());

        if(movies == null || movies.size() == 0) {
            return;
        }

        Movie movie = movies.get(0);

        movie.setTitle(movieViewModel.getTitle());
        movie.setPosterUrl(movieViewModel.getPosterUrl());
        movie.setRuntime(movieViewModel.getRuntime());
        movie.setReleased(movieViewModel.getReleased());
        movie.setImdbID(movieViewModel.getImdbID());
        movie.setTitle(movieViewModel.getTitle());

        movie.update();
    }

    @Override
    public void createMovie(MovieViewModel movieViewModel) {
        if(movieViewModel == null) {
            return;
        }

        Movie movie = viewModelMapper.mapViewModelToDB(movieViewModel);
        movieDao.insert(movie);
    }

    @Override
    public LazyList<Movie> getAllMovies() {
        return movieDao.queryBuilder().listLazy();
    }

    private List<Movie> getMoviesByID(String imdbID) {
        return movieDao.queryBuilder().where(MovieDao.Properties.ImdbID.eq(imdbID)).list();
    }

}
