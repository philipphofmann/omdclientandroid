package philipphofmann.at.moviebrowser.provider;

/**
 * Builds request urls for the api of http://www.omdbapi.com/
 *
 * Created by philipphofmann on 21/09/15.
 */
public interface OmdRequestUrlBuilder {
    /**
     * Builds the request url for the json request against the OmdbApi for getting a movie by imdbID.
     * @param imdbID The imdbID of the movie to search.
     * @return The url for the json request.
     */
    String getByImdbIDUrl(String imdbID);

    /**
     * Builds the request url for the json request against the OmdbApi for searching a list of movies
     * by the given title.
     * @param title The title of the movies to search.
     * @return The url for the json request.
     */
    String getMultipleByTitleUrl(String title);
}
