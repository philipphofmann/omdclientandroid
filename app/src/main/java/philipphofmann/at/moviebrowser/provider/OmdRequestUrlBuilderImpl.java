package philipphofmann.at.moviebrowser.provider;

import android.util.Log;

import org.androidannotations.annotations.EBean;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by philipphofmann on 21/09/15.
 */
@EBean
public class OmdRequestUrlBuilderImpl implements OmdRequestUrlBuilder {

    private static final String TAG = OmdRequestUrlBuilderImpl.class.getSimpleName();

    private static final String getByImdbIDFormat = "http://www.omdbapi.com/?i=%s&y=&plot=full&r=json";
    private static final String getMultipleByTitleFormat = "http://www.omdbapi.com/?s=%s&y=&plot=full&r=json";

    @Override
    public String getByImdbIDUrl(String imdbID) {
        return getUrl(getByImdbIDFormat, imdbID);
    }

    @Override
    public String getMultipleByTitleUrl(String title) {
        return getUrl(getMultipleByTitleFormat, title);
    }

    private String getUrl(String formatString, String formatParameter) {
        try {
            String titleEncoded = URLEncoder.encode(formatParameter, "UTF-8");
            return String.format(formatString, titleEncoded);
        } catch (UnsupportedEncodingException e) {
            Log.e(TAG, "Encoding not supported", e);
        }

        return "";
    }
}
