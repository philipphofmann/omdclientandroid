package philipphofmann.at.moviebrowser.provider;

import android.graphics.Bitmap;

import philipphofmann.at.moviebrowser.base.BaseAsyncTask;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieSearchViewModel;
import philipphofmann.at.moviebrowser.dataobjects.view.MovieViewModel;

/**
 * Provides data calls to the <a href="http://www.omdbapi.com/">OmdbAPI</a>
 *
 * Created by philipphofmann on 19/09/15.
 */
public interface OmdDataProvider {

    /**
     * Makes an json request to the <a href="http://www.omdbapi.com/">OmdbAPI</a> to the movies with
     * the given imdbID.
     * @param title The title of the movie to search for.
     */
    MovieViewModel getMovieByImdbID(String title);

    /**
     * Makes an json request to the <a href="http://www.omdbapi.com/">OmdbAPI</a> to the movies with
     * the given title.
     * @param title The title of the movie to search for.
     */
    MovieSearchViewModel getMoviesByTitle(String title);

    /**
     * Builds an {@link android.os.AsyncTask} that downloads the passed image.
     * @param imageUrl The url of the image to download.
     * @return An {@link android.os.AsyncTask} to execute.
     */
    BaseAsyncTask<Void, Bitmap> getImageByUrl(String imageUrl);

    void callMeOnResume();

    void callMeOnPause();
}
